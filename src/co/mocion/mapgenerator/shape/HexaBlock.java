package co.mocion.mapgenerator.shape;

import javafx.geometry.Point3D;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

import java.util.ArrayList;

public class HexaBlock extends MeshView {

    public final static float SEGMENT_SIZE = 0.125f;

    private TriangleMesh triangleMesh;
    private float radio = 50.35f; // 0.35cm
    private int segments = 1; // 0.125 cm

    public HexaBlock() {
        setMesh(createObject(radio, segments));
    }

    private TriangleMesh createObject (float radio, int segments) {
        TriangleMesh triangleMesh = new TriangleMesh();

        // TOP CENTER POINT
        triangleMesh.getPoints().setAll( 0.0f, 0.0f, 0.0f);

        for (int i = 0; i<= segments; i++) {
            for (int j = 0; j < 6; j++) {
                triangleMesh.getPoints().addAll(
                        (float) Math.sin(Math.PI * j / 3) * radio,
                        ((float) i) * SEGMENT_SIZE,
                        (float) Math.cos(Math.PI * j / 3) * radio
                );
            }
        }

        // BOTTOM CENTER POINT
        triangleMesh.getPoints().addAll(0.0f, ((float) segments) * SEGMENT_SIZE, 0.0f);

        float textureU = 0.5f;
        float textureV = 0.5f;

        // CENTER UV COORDINATES
        triangleMesh.getTexCoords().setAll(textureU, textureV);

        // BORDER UV COORDINATES
        for (int j = 0; j < 6; j++) {
            textureU = 0.5f + ((float) Math.sin((Math.PI * (double) (2 * j)) / (double) 6)) * 0.5f;
            textureV = 0.5f + ((float) Math.cos((Math.PI * (double) (2 * j)) / (double) 6)) * 0.5f;
            triangleMesh.getTexCoords().addAll(textureU, textureV);
        }

        // SIDES UV COORDINATES
        triangleMesh.getTexCoords().addAll(0.0f, 0.0f, 0.0f, 1.0f);

        // TOP FACES
        int vertices = (triangleMesh.getPoints().size() / 3);
        for (int k = 0; k < vertices; k++) {
            if (k < 6) {
                if (k == 4) {
                    triangleMesh.getFaces().addAll(0, 0, k + 1, k + 1, (k + 2), (k + 2));
                }
                else {
                    triangleMesh.getFaces().addAll(0, 0, k + 1, k + 1, (k + 2) % 6, (k + 2) % 6);
                }
            }

            if (k > 0 && k < vertices - 1) {

            }

        }

        // SIDE FACES
        // PENDING

        // BOTTOM FACES
//        int maxPoint = triangleMesh.getPoints().size() - 1;
//        for (int k = 0; k < 6; k++) {
//            if (k == 4) {
//                triangleMesh.getFaces().addAll(
//                        maxPoint, 0,
//                        maxPoint - k - 1, k + 1,
//                        maxPoint - k - 2, (k + 2)
//                );
//            }
//            else {
//                triangleMesh.getFaces().addAll(
//                        maxPoint, 0,
//                        maxPoint - k - 1, k + 1,
//                        (maxPoint - k - 2) % 6, (k + 2) % 6);
//            }
//        }

        return triangleMesh;
    }

    public void update() {
        setMesh(createObject(radio, segments));
    }

    public float getRadio() {
        return radio;
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }

    public int getSegments() {
        return segments;
    }

    public void setSegments(int segments) {
        this.segments = Math.abs(segments);
    }
}

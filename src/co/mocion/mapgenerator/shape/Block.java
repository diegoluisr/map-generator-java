package co.mocion.mapgenerator.shape;

import javafx.geometry.Point3D;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

public class Block extends MeshView {

    private TriangleMesh triangleMesh;

    private Point3D baseCenter;
    private Point3D topCenter;

    private Point3D[] bottom = new Point3D[6];
    private Point3D[] top = new Point3D[6];

    private double height = 1.0;
    private double radio = 1.0;

    public Block() {
        setMesh(createObject(50.0f, 100.0f));
    }

    public TriangleMesh createObject (float radio, float height) {
        TriangleMesh triangleMesh = new TriangleMesh();

        float[] pointsX = new float[7];
        float[] pointsY = new float[7];
        float[] pointsZ = new float[7];

        float[] textureU = new float[7];
        float[] textureV = new float[7];

        pointsX[0] = 0.0f;
        pointsY[0] = 0.0f;
        pointsZ[0] = 0.0f;

        for (int i = 1; i < pointsX.length; i++) {
            pointsX[i] = (float) Math.sin(Math.PI * 2 * i / 6) * radio;
            pointsY[i] = (float) Math.cos(Math.PI * 2 * i / 6) * radio;
            pointsZ[i] = 0.0f;
        }

        triangleMesh.getPoints().addAll(combine3Arrays(pointsX, pointsY, pointsZ));

        textureU[0] = 0.5f;
        textureV[0] = 0.5f;

        for (int j = 1; j < textureU.length; j++) {
            textureU[j] = 0.5f + ((float) Math.sin((Math.PI * (double) (2 * j)) / (double) 6)) * 0.5f;
            textureV[j] = 0.5f + ((float) Math.cos((Math.PI * (double) (2 * j)) / (double) 6)) * 0.5f;
        }

        triangleMesh.getTexCoords().setAll(combine2Arrays(textureU, textureV));

        triangleMesh.getFaces().addAll(
                0, 0, 1, 1, 2, 2,
                0, 0, 2, 2, 3, 3,
                0, 0, 3, 3, 4, 4,
                0, 0, 4, 4, 5, 5,
                0, 0, 5, 5, 6, 6,
                0, 0, 6, 6, 1, 1
        );

        return triangleMesh;
    }

    public float[] combine2Arrays(float[] a, float[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("Asymetric arrays.");
        }
        float[] combined = new float[a.length * 2];

        for (int i = 0; i < a.length; i++) {
            combined[i*2] = a[i];
            combined[i*2+1] = b[i];
        }

        return combined;
    }

    public float[] combine3Arrays(float[] a, float[] b, float[] c) {
        if (a.length != b.length || b.length != c.length) {
            throw new IllegalArgumentException("Asymetric arrays.");
        }
        float[] combined = new float[a.length * 3];

        for (int i = 0; i < a.length; i++) {
            combined[i*3] = a[i];
            combined[i*3+1] = b[i];
            combined[i*3+2] = c[i];
        }

        return combined;
    }

}

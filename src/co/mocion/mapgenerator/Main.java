package co.mocion.mapgenerator;

import co.mocion.mapgenerator.shape.Block;
import co.mocion.mapgenerator.shape.HexaBlock;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.DrawMode;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    final Group root = new Group();
    final Group items = new Group();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Scene scene = new Scene(root, 1024, 768, true);
        scene.setFill(Color.GREY);

        primaryStage.setTitle("Planes");
        primaryStage.setScene(scene);
        primaryStage.show();

        PerspectiveCamera camera = new PerspectiveCamera(false);
//        camera.setTranslateX(0);
//        camera.setTranslateY(-600);
        camera.setTranslateZ(-800);

        //camera.setRotationAxis(new Point3D(0, -0.5f, 0));
        //camera.setRotate(180);

        scene.setCamera(camera);

        Image difuseMap = new Image(getClass().getResource("/res/images/hexagon_green.png").toExternalForm());

        for(int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                HexaBlock block = new HexaBlock();
                block.setDrawMode(DrawMode.LINE);
//                block.setRotate(30);
                block.setTranslateZ(20 + (75 * i));
                if (i % 2 == 1) {
                    block.setTranslateX((86 * j));
                }
                else {
                    block.setTranslateX(43.0 + (86 * j));
                }

                block.setMaterial(new PhongMaterial(Color.WHITE, difuseMap, null, null, null));

                items.getChildren().add(block);
            }
        }

        root.getChildren().add(items);

//        items.setRotationAxis(new Point3D(1, 0, 0));
//        items.setRotate(-180);
        items.setRotationAxis(new Point3D(0.25, 0.5, 0.75));
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(40), event -> {
            items.setRotate(items.getRotate() + 1);
        }));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
